FROM cypress/browsers:node12.14.1-chrome85-ff81

ADD . /src

WORKDIR /src

RUN cd /src \
	&& npm install \
	&& npx cypress install

CMD npx cypress run --browser chrome --config video=false
